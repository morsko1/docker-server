FROM node:14-alpine AS development
ENV NODE_ENV development
# Add a work directory
WORKDIR /app
# Cache and Install dependencies
COPY package*.json .
RUN npm install
# Copy app files
COPY . .
# Expose port
EXPOSE 5555
# Start the app
CMD [ "npm", "start" ]

FROM node:14-alpine AS builder
ENV NODE_ENV production
# Add a work directory
WORKDIR /app
# Cache and Install dependencies
COPY package*.json .
RUN npm install --production
# Copy app files
COPY . .
# Build
CMD npm run build

FROM node:14-alpine AS production
# Copy built assets/bundle from the builder
COPY --from=builder /app .
EXPOSE 80
# Start the app
CMD node index.js
