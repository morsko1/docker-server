const express = require('express');
const app = express();
const port = 5555;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.get('/api/data', (req, res) => {
  console.log('request ---');
  res.send({"data": "Hello from Node app!"});
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
